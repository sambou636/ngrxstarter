import { InMemoryDbService } from "angular-in-memory-web-api";
import { User } from '../model/user';
import { Book } from "../model/books";

export class UsersData implements InMemoryDbService {
    createDb(): Record<string, User[] | Book[]> {
        const users: User[] = [
            {
                id: 1,
                username: "Enock",
                isAdmin: false
            },
            {
                id: 2,
                username: "Yvan",
                isAdmin: true
            },
            {
                id: 3,
                username: "Romaric",
                isAdmin: false
            }
        ];
        // return { users };
        const books: Book[] = [
            {
                id: 1,
                name: 'Learn NGRX',
                author: 'Manner',
                publisher: 'Lamine'
            },
        ];
        return { users, books };
    }
}