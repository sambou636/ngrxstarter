import { InMemoryDbService } from "angular-in-memory-web-api";
import { Book } from "../model/books";
import { User } from '../model/user';

export class Data implements InMemoryDbService {
    createDb(): Record<string, User[] | Book[]> {
        const users: User[] = [
            {
                id: 1,
                username: "Enock",
                isAdmin: false
            },
            {
                id: 2,
                username: "Yvan",
                isAdmin: true
            },
            {
                id: 3,
                username: "Romaric",
                isAdmin: false
            }
        ];
        // return { users };
        const books: Book[] = [
            {
                id: 1,
                name: 'Learn NGRX',
                author: 'Manner',
                publisher: 'Lamine'
            },

            {
                id: 2,
                name: 'Learn Java',
                author: 'Frederic',
                publisher: 'Adama'
            },

            {
                id: 3,
                name: 'Learn Php',
                author: 'Antoine',
                publisher: 'Marie'
            },



        ];
        return { users, books };
    }

    generateID(books: Book[]): number {
        return books.length > 0 ? Math.max(...books.map(book => book.id!)) + 1 : 1;
    }
}