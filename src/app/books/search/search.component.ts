import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { addBook, addBookSuccess, loadBooks, searchBookSuccess, updateBook } from '../state/actions/books/books.actions';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent {

  public booksForm!: FormGroup;

  constructor(private formBuilder: FormBuilder, private store: Store) {

  }

  ngOnInit() {
    this.booksForm = this.formBuilder.group({
      search: [''],
    });
  }

  onSubmit() {
    console.log('form:', this.booksForm.value);
    const name: string = this.booksForm.value.search;
    if (name == "") {
      this.store.dispatch(loadBooks());
    } else {
      this.store.dispatch(searchBookSuccess({ name }));
    }


  }


}
