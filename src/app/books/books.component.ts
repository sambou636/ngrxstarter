import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Book } from '../model/books';
import * as bookAction from '../books/state/actions/books/books.actions';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  public books$!: Observable<Book[]>;

  constructor(private store: Store) {

  }

  ngOnInit() {
    this.store.dispatch(bookAction.loadBooks());
  }


}
