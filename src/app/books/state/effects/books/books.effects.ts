import { Injectable } from "@angular/core";
import * as bookAction from '../../../../books/state/actions/books/books.actions';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { tap, mergeMap, map, catchError, of } from "rxjs";
import { BooksService } from "../../../service/books.service";
import { Book } from "../../../../model/books";


@Injectable()

export class BooksEffects {

    constructor(private action$: Actions, private bookservice: BooksService) {

    }

    loadBook$ = createEffect(() =>
        this.action$.pipe(
            tap((val) => console.log('action :', val)),
            ofType(bookAction.loadBooks),
            mergeMap(action => this.bookservice.getBooks().pipe(
                map((books: Book[]) => bookAction.loadBooksSuccess({ books })),
                catchError(error => of(bookAction.loadBooksFailure({ error: error.body.error })))
            )))
    );


    addBook$ = createEffect(() =>
        this.action$.pipe(
            ofType(bookAction.addBook),
            mergeMap(({ book }) => this.bookservice.addBooks(book).pipe(
                map((book: Book) => bookAction.addBookSuccess({ book })),
                catchError(error => of(bookAction.loadBooksFailure({ error: error.body.error })))
            )))

    );

    deleteBook$ = createEffect(() =>
        this.action$.pipe(
            ofType(bookAction.deleteBook),
            mergeMap(({ id }) => this.bookservice.deleteBooks(id).pipe(
                map(() => bookAction.deleteBookSuccess({ id })),
                catchError(error => of(bookAction.deleteBookFailed({ error: error.body.error })))
            )))

    );

    updateBook$ = createEffect(() =>
        this.action$.pipe(
            ofType(bookAction.updateBook),
            mergeMap(({ book }) => this.bookservice.updateBooks(book).pipe(
                // map((book: Book) => bookAction.updateBookSuccess({ book })),
                map(() => bookAction.updateBookSuccess({ book })),
                catchError(error => of(bookAction.updateBookFailed({ error: error.body.error })))
            )))

    );






}
