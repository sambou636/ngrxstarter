import { Action, ActionReducer, createReducer, on } from '@ngrx/store';
import { Book } from 'src/app/model/books';
import * as bookAction from '../../../../books/state/actions/books/books.actions';

export const bookFeatureKey = 'books';

export interface BooksState {
  books: Book[];
  selectedBook?: Book;
}


export interface State {
  readonly [bookFeatureKey]: BooksState
}

export const initialState: BooksState = {
  books: []
};

export const reducers = createReducer(
  initialState,
  on(bookAction.loadBooksSuccess, (state, { books }) => {
    console.log('test');
    return {
      ...state,
      books
    }

  }),
  on(bookAction.addBook, (state) => {
    return {
      ...state
    }
  }),
  on(bookAction.addBookSuccess, (state, { book }) => {
    console.log('livre :', book);
    return {
      ...state,
      books: [...state.books, book]
    }

  }),
  on(bookAction.deleteBookSuccess, (state, { id }) => {
    console.log('ID livre :', id);
    return {
      ...state,
      books: state.books.filter((book: Book) => book.id != id)
    }

  }),
  on(bookAction.updateBook, (state, { book }) => {
    return {
      ...state,
    }

  }),
  on(bookAction.updateBookSuccess, (state, { book }) => {
    console.log('modif reducer livre :', book);
    const updatedBooks: Book[] = state.books.map((existingBook: Book) =>
      existingBook.id == book.id ? book : existingBook);

    return {
      ...state,
      books: updatedBooks
    }

  }),
  on(bookAction.searchBookSuccess, (state, { name }) => {
    console.log('Nom livre :', name);
    return {
      ...state,
      books: state.books.filter((book: Book) => book.name == name)
    }

  }),
);