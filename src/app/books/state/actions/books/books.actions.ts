import { createAction, props } from '@ngrx/store';
import { Book } from 'src/app/model/books';

export const loadBooks = createAction(
  '[Books] Load Books'
);

export const loadBooksSuccess = createAction(
  '[Books] Load Books Success',
  props<{ books: Book[] }>()
);

export const loadBooksFailure = createAction(
  '[Books] Load Books Failure',
  props<{ error: any }>()
);


export const addBook = createAction(
  '[Books] Add Book',
  props<{ book: Book }>()
);

export const addBookSuccess = createAction(
  '[Books] Add Book Success',
  props<{ book: Book }>()
);

export const addBookFailure = createAction(
  '[Books] Add Book Failure',
  props<{ error: any }>()
);

export const deleteBook = createAction(
  '[Books] Delete Book',
  props<{ id: number }>()
);


export const deleteBookSuccess = createAction(
  '[Books] Delete Book Success',
  props<{ id: number }>()
);

export const deleteBookFailed = createAction(
  '[Books] Delete Book Failed',
  props<{ error: any }>()
);

export const updateBook = createAction(
  '[Books] Update Book',
  props<{ book: Book }>()
);


export const updateBookSuccess = createAction(
  '[Books] Update Book Success',
  props<{ book: Book }>()
);

export const updateBookFailed = createAction(
  '[Books] Update Book Failed',
  props<{ error: any }>()
);

export const searchBookSuccess = createAction(
  '[Books] Search Book Success',
  props<{ name: string }>()
);


