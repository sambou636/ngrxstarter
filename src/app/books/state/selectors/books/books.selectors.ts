import { createFeatureSelector, createSelector } from '@ngrx/store';
import { Book } from 'src/app/model/books';
import { bookFeatureKey, BooksState } from '../../reducers/books/books.reducer';


export const selectBooksFeature = createFeatureSelector<BooksState>(bookFeatureKey);
export const getBooks = createSelector(selectBooksFeature, (state: BooksState) => state.books);
