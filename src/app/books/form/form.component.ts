import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { addBook, addBookSuccess, updateBook } from '../state/actions/books/books.actions';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  public booksForm!: FormGroup;

  constructor(private formBuilder: FormBuilder, private store: Store) {

  }

  ngOnInit() {
    this.booksForm = this.formBuilder.group({
      id: [''],
      name: [''],
      // name: ['', Validators.minLength(8), Validators.required],
      publisher: [''],
      author: ['']

    });
  }

  resetForm() {
    this.booksForm.reset();
    this.booksForm = this.formBuilder.group({
      id: [''],
      name: [''],
      publisher: [''],
      author: ['']

    });
  }

  onSubmit() {
    console.log('form:', this.booksForm.value);
    //this.store.dispatch(addBook({ book: this.booksForm.value }))
    if (this.booksForm.value.id != "") {
      this.store.dispatch(updateBook({ book: this.booksForm.value }));

    } else {
      this.store.dispatch(addBook({ book: this.booksForm.value }));

    }
    this.resetForm();
  }

}
