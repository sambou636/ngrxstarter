import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { bookFeatureKey, reducers } from './state/reducers/books/books.reducer';
import { EffectsModule } from '@ngrx/effects';
import { BooksEffects } from './state/effects/books/books.effects';
import { RouterModule } from '@angular/router';
import { BooksComponent } from './books.component';
import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SearchComponent } from './search/search.component'




@NgModule({
  declarations: [
    FormComponent, BooksComponent, ListComponent, SearchComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    StoreModule.forFeature(bookFeatureKey, reducers),
    EffectsModule.forFeature([BooksEffects]),
    RouterModule.forChild([
      {
        path: '',
        component: BooksComponent
      }])
  ],
  exports: [FormComponent, BooksComponent, ListComponent]
})
export class BooksModule { }
