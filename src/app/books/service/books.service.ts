import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { delay, Observable } from 'rxjs';
import { Book } from 'src/app/model/books';
import { User } from 'src/app/model/user';
import { Data } from '../../api/data';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  constructor(private http: HttpClient) { }

  public getBooks(): Observable<Book[]> {
    /*return this.http.get<Book[]>('api/books').pipe(
      delay(3000)
    );*/
    return this.http.get<Book[]>('api/books');

  }

  public addBooks(book: Book): Observable<Book> {
    const body: Book = { ...book, id: null };
    console.log('service add book : ', { ...book });
    return this.http.post<Book>('api/books', body);
  }

  public deleteBooks(id: number): Observable<{}> {
    console.log('service id deleted : ', id);
    return this.http.delete<Book>('api/books/' + id);
  }

  public updateBooks(book: Book): Observable<Book> {
    //const body: Book = { ...book, id: null };
    console.log('service update book : ', { ...book });
    return this.http.put<Book>('api/books/' + book.id, book);
  }


}
