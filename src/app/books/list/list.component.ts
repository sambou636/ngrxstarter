import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Book } from 'src/app/model/books';
import { deleteBook } from '../state/actions/books/books.actions';
import { BooksState } from '../state/reducers/books/books.reducer';
import { getBooks } from '../state/selectors/books/books.selectors';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  public books$: Observable<Book[]> = {} as Observable<Book[]>;

  @Input() bookFormFromList!: FormGroup;

  constructor(private store: Store) {

  }

  ngOnInit() {
    this.books$ = this.store.select(getBooks);
  }

  deleteBook(id: any) {
    this.store.dispatch(deleteBook({ id }));
  }

  editBook(book: Book) {
    console.log(book);
    this.bookFormFromList.patchValue({
      id: book.id,
      name: book.name,
      publisher: book.publisher,
      author: book.publisher

    })
  }

}
