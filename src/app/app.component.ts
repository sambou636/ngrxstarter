import { Component, OnInit, SimpleChanges } from '@angular/core';
import { Store, select } from '@ngrx/store';
//import { initAction, changeUsernameAction, isAdminAction } from './state/01-action';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { State, RootState } from '../app/model/state';
import { User } from '../app/model/user';
import { getUser, getUsers, getLoaded, getError } from '../app/state/02-selectors';
import * as RootActions from './state/01-action';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent implements OnInit {
  title = 'ngrxStarter';
  public form!: UntypedFormGroup;
  public user$: Observable<RootState> = {} as Observable<RootState>;
  public users$!: Observable<User[]>;
  public isloaded$!: Observable<Boolean>;
  public error$: Observable<HttpErrorResponse | Error | string> = {} as Observable<HttpErrorResponse | Error | string>;

  constructor(private store: Store<RootState>, private http: HttpClient) {

  }

  ngOnInit() {
    this.store.dispatch(RootActions.initAction());

    this.form = new UntypedFormGroup({
      name: new UntypedFormControl('', Validators.compose([]))
    });
    //1ere methode
    //this.user$ = this.store.select((state: any) => state.root1.user);
    //2eme methode
    //this.user$ = this.store.pipe(select((state: State) => state));
    //3eme methode
    this.user$ = this.store.pipe(select(getUser));

    //this.http.get('api/users').subscribe(val => console.log(val));
    //console.log(this.user$);
    //get all user
    this.store.dispatch(RootActions.loadUsers());
    this.users$ = this.store.pipe(select(getUsers));

    //get state of loaded
    this.isloaded$ = this.store.pipe(select(getLoaded));

    //get error of api
    this.error$ = this.store.pipe(select(getError));


  }

  changerUsername() {

    if (this.form.valid) {
      this.store.dispatch(RootActions.changeUsernameAction({ username: this.form.value.name }));
      // this.store.dispatch(RootActions.isAdminAction({ isAdmin: true }));

    } else {
      console.log('Error: Form invalid');
    }
    // this.store.dispatch(changeUsername({ username: 'papa' }));

  }

  loadUsers() {
    this.store.dispatch(RootActions.loadUsers());

  }
}
