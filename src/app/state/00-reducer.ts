import { createReducer, MetaReducer, ActionReducer, on, Action } from '@ngrx/store';
import { type } from 'os';
//import { changeUsernameAction, initAction } from '../state/01-action';
import * as RootActions from '../state/01-action';
import { State, RootState } from '../model/state';
import { HttpErrorResponse } from '@angular/common/http';

export const ROOT_FEATURE_KEY = 'root1';

const initialState: RootState = {
    appName: "Ngrx First Application",
    user: {
        username: '',
        isAdmin: false,
    },

    users: [],
    loaded: false,
    error: ''
};

// console.log all actions
function log(reducer: ActionReducer<State>): ActionReducer<State> {
    return (state, action) => {

        console.groupCollapsed(action.type)
        console.log('state', state);
        console.log('action', action);
        console.log('after state', reducer(state, action));
        console.groupEnd();

        return reducer(state, action);
    }
}


export const metaReducers: MetaReducer[] = [log];

export const rootReducer = createReducer<RootState, Action>(initialState,
    on(RootActions.initAction, (state: RootState) => {
        return {
            ...state,
            user: {
                ...state.user,
                isAdmin: true
            }

        }
    }),
    on(RootActions.changeUsernameAction, (state: RootState, props) => {
        return {
            ...state,
            user: {
                ...state.user,
                username: props.username,
                isAdmin: true
            }
        }
    }),
    on(RootActions.loadUsers, (state: RootState) => {
        return {
            ...state,
            loaded: false
        }
    }),
    on(RootActions.loadUsersSuccess, (state: RootState, props) => {
        return {
            ...state,
            users: props.users,
            loaded: true
        }
    }),
    on(RootActions.loadUsersError, (state: RootState, props) => {
        return {
            ...state,
            users: [],
            loaded: true,
            error: props.error
        }
    }),

);