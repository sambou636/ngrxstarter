import { Injectable } from "@angular/core";
import * as UsersAction from '../state/01-action';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { tap, mergeMap, map, catchError, of } from "rxjs";
import { UsersService } from "../services/users/users.service";
import { User } from "../model/user";

@Injectable()
export class AppEffects {

    /*loadUsers$ = createEffect(() =>
        this.action$.pipe(
            tap((val) => console.log('action :', val)),
            ofType(UsersAction.loadUsers),
            mergeMap(action => this.userservice.getUsers().pipe(
                map((users: User[]) => UsersAction.loadUsersSuccess({ users })),
                catchError(error => of(UsersAction.loadUsersError({ error: error.body.error })))
            )))
    );*/

    constructor(private action$: Actions, private userservice: UsersService) {

    }

}