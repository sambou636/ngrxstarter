import { createAction, props } from '@ngrx/store';
import { User } from '../model/user';
import { HttpErrorResponse } from '@angular/common/http';

export const initAction = createAction('[ROOT1] init App');

export const changeUsernameAction = createAction('[ROOT1] changer username', props<{ username: string }>());

export const isAdminAction = createAction('[ROOT1] changer isAdmin', props<{ isAdmin: boolean }>());

export const loadUsers = createAction('[Users API] load users');

export const loadUsersSuccess = createAction('[Users API] load users success',
    props<{ users: User[] }>());

export const loadUsersError = createAction('[Users API] load users error',
    props<{ error: HttpErrorResponse | Error | string }>());
