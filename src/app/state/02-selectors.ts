import { createSelector, Selector, createFeatureSelector } from '@ngrx/store';
import { RootState, State } from '../model/state';
import { ROOT_FEATURE_KEY } from '../state/00-reducer';


//si on utiliser l'observable User pour accéder seulement à la classe User dans app.compoenent.ts, decommentez ce code
/*const selectRoot = (state: State) => state.root1;

export const getUser = createSelector(selectRoot, (state1: RootState) => (state1.user));*/

//const selectRoot = (state: State) => state;
const selectRoot = createFeatureSelector<RootState>(ROOT_FEATURE_KEY);

export const getUser = createSelector(selectRoot, (state1: RootState) => (state1));

export const getUsers = createSelector(selectRoot, (state1: RootState) => (state1.users));

export const getLoaded = createSelector(selectRoot, (state1: RootState) => (state1.loaded));

export const getError = createSelector(selectRoot, (state1: RootState) => (state1.error));