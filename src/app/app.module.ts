import { NgModule, isDevMode, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StoreModule } from '@ngrx/store';
import { rootReducer, metaReducers, ROOT_FEATURE_KEY } from './state/00-reducer';
import { ReactiveFormsModule } from '@angular/forms';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { AppEffects } from './state/04-effect';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
//import { UsersData } from './api/user.data';
import { Data } from './api/data';
import { HttpClientModule } from '@angular/common/http';
import { BooksModule } from '../app/books/books.module';




@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    InMemoryWebApiModule,
    BooksModule,
    HttpClientModule,
    StoreModule.forRoot({ [ROOT_FEATURE_KEY]: rootReducer },
      {
        metaReducers: metaReducers,
        runtimeChecks: {
          strictActionTypeUniqueness: true,
          strictActionImmutability: true,
          strictStateImmutability: true
        }
      }),
    StoreDevtoolsModule.instrument({
      name: 'ngrxStarter',
      maxAge: 25, //nombre d'action max à afficher dans la console...on peut le modifier aussi
      logOnly: !isDevMode()
    }),
    EffectsModule.forRoot([AppEffects]),
    // InMemoryWebApiModule.forRoot(UsersData),
    InMemoryWebApiModule.forRoot(Data),
    AppRoutingModule

  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
