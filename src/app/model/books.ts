

export interface Book {
    id: number | null;
    name: string;
    author: string;
    publisher: string;
}