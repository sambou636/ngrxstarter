import { User } from './user';
import { ROOT_FEATURE_KEY } from '../state/00-reducer';
import { HttpErrorResponse } from '@angular/common/http';

export interface State {
    //root1 est considéré comme une clé.....ex : state.root1.user.username ou root1 est la clé
    readonly [ROOT_FEATURE_KEY]: RootState;
}


export interface RootState {
    appName: string;
    user: User;
    users: User[];
    loaded: boolean;
    error: HttpErrorResponse | Error | string;
}

//ceci est une indication
/*interface State = interface RootState + interface  User{
    root1: {
        appName: string;
        user: User;
    }
}*/